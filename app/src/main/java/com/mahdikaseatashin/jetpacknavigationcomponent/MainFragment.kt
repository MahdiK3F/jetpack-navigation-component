package com.mahdikaseatashin.jetpacknavigationcomponent

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.NavController
import androidx.navigation.Navigation

class MainFragment : Fragment(), View.OnClickListener {

    lateinit var navController : NavController

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)
        view.findViewById<Button>(R.id.btn_main_1).setOnClickListener(this)
        view.findViewById<Button>(R.id.btn_main_2).setOnClickListener(this)
        view.findViewById<Button>(R.id.btn_main_3).setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v!!.id){
            R.id.btn_main_1 -> navController.navigate(R.id.action_mainFragment2_to_viewTransactionFragment2)
            R.id.btn_main_2 -> navController.navigate(R.id.action_mainFragment2_to_chooseRecipientFragment2)
            R.id.btn_main_3 -> navController.navigate(R.id.action_mainFragment2_to_viewBalanceFragment2)
        }
    }
}
